@extends('layout.app')

@section('content')
        <div class="container-fluid no-padding hero-container">
          <div class="hero-image">
            <div class="hero-title">
              <h1>Speech2Text.co.uk</h1>
            </div>
            <div class="hero-subtitle">
              <div class="output" id="output">
                <h1 class="cursor"></h1>
                <p></p>
              </div>
            </div>
          </div>
        </div>

        <section id="instant" class="container-fluid front-blurb-container">
          <div class="container my-4">
            <div class="row front-blurb-text">
              <div class="col-12 col-md-6">
                <h2>Get an Instant Price and Risk Free Service</h2>
                <ol class="front-blurb">
                  <li>Upload your audio or video file using the form</li>
                  <li>Receive your instant guaranteed price</li>
                  <li>Submit your job</li>
                  <li>When your transcript is ready you'll receive an excerpt so you can check the quality</li>
                  <li>Pay the balance and receive your full transcript</li>
                </ol>
              </div>

              <div class="col-12 col-md-6">
                <div class="card front-upload-card">
                  <div class="card-body">
                    <audio id="audio"></audio>
                    <form method="POST" action="{{ route('file.store') }}" enctype="multipart/form-data" id="upload-form">
                      @csrf
                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="uploadFile">
                            Upload your file
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="file" name="uploadFile" id="uploadFile" required />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="name">
                            Your name
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="text" name="name" id="name" required />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="email">
                            Email
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="email" name="email" id="email" required />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="phone">
                            Telephone
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="text" name="phone" id="phone" />
                        </div>
                      </div>

                      <div class="form-row mt-2">
                        <div class="col-6 col-md-4">
                          <label for="turnaround">
                            Turnaround
                          </label>
                        </div>
                        <div class="col-6 col-md-8">
                            <select name="turnaround" id="turnaround">
                              <option value="0">5 hours</option>
                              <option value="1">1 day</option>
                              <option value="2">2 days</option>
                              <option value="3">3 days</option>
                              <option value="4">4 days</option>
                            </select>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-6 col-md-4">
                          <label for="verbatim">
                            Verbatim
                          </label>
                        </div>
                        <div class="col-6 col-md-8">
                          <input type="checkbox" name="verbatim" id="verbatim" value="true" />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-6 col-md-4">
                          <label for="timestamps">
                            Timestamps
                          </label>
                        </div>
                        <div class="col-6 col-md-8">
                          <input type="checkbox" name="timestamps" id="timestamps" value="true" />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-6 col-md-4">
                          <label for="duration">
                            File duration
                          </label>
                        </div>
                        <div class="col-6 col-md-8">
                            <span id="duration"><i>Ready to calculate</i></span>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-6 col-md-4">
                          <label for="cost">
                            Guaranteed cost
                          </label>
                        </div>
                        <div class="col-6 col-md-8">
                            <span id="cost"><i>Ready to calculate</i></span>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-6 col-md-4">
                        </div>
                        <div class="col-6 col-md-8">
                            <button type="submit" class="btn btn-primary">SEND NOW</button>
                        </div>
                      </div>

                    </form>

                  </div><!-- /card-body -->
                </div><!-- /card -->
              </div>
            </div><!-- /row -->
          </div><!-- /container -->
        </section><!-- /container-fluid -->
        <div class="front-blurb-background"></div><!-- /front-blurb-background -->

        <section id="pricing" class="container-fluid front-second-container">
          <div class="container my-4">
            <div class="row">
              <div class="col-12 col-md-6">
                <h2>Pricing</h2>
                <p>We keep it simple so you know exactly what you can expect to pay</p>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-6">
                <table class="table">
                  <tr>
                    <th></th>
                    <th colspan="2">Price per minute of audio/video</th>
                  </tr>
                  <tr>
                    <th></th>
                    <th>Standard Price</th>
                    <th><span class="gold">Gold Member</span></th>
                  </tr>
                  <tr>
                    <td>4 day turnaround</td>
                    <td>£0.92</td>
                    <td>£0.80</td>
                  </tr>
                  <tr>
                    <td>3 day turnaround</td>
                    <td>£1.10</td>
                    <td>£0.95</td>
                  </tr>
                  <tr>
                    <td>2 day turnaround</td>
                    <td>£1.70</td>
                    <td>£1.12</td>
                  </tr>
                  <tr>
                    <td>1 day turnaround</td>
                    <td>£2.50</td>
                    <td>£1.95</td>
                  </tr>
                  <tr>
                    <td>5 hour turnaround</td>
                    <td>£3.20</td>
                    <td>£2.80</td>
                  </tr>
                  <tr>
                    <td>Minimum charge</td>
                    <td>£2.99</td>
                    <td>£1.99</td>
                  </tr>
                </table>
              </div>

              <div class="col-12 col-md-6">
                <table class="table">
                  <tr>
                    <th></th>
                    <th colspan="2">Optional extras</th>
                  </tr>
                  <tr>
                    <th></th>
                    <th>Standard Price</th>
                    <th><span class="gold">Gold Member</span></th>
                  </tr>
                  <tr>
                    <td>Verbatim</td>
                    <td>+£0.15/min</td>
                    <td>+£0.10/min</td>
                  </tr>
                  <tr>
                    <td>Timestamps</td>
                    <td>+£0.15/min</td>
                    <td>+£0.10/min</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </section><!-- /pricing -->

        <section id="gold-members" class="container-fluid front-golden-container">
          <div class="container my-4 golden-body">
            <div class="row">
              <div class="col-12 col-md-6" style="margin-left: auto; margin-right: auto; max-width: 90vw;">
                <h2>Gold Members</h2>
                <p>We believe in rewarding customer loyalty, which is why after submitting 160 hours of work
                  you'll automatically become a Gold Member, with access to exclusive discounts.</p>
              </div>
            </div>
          </div>
        </section><!-- /golden -->

        <section id="contact" class="container-fluid front-message-container">
          <div class="container my-4">
            <div class="row front-message-text">
              <div class="col-12 col-md-6">
                <h2>Contact Us</h2>
                <p>Feel free to get in touch with any questions you have and we'll get right back to you</p>
              </div>

              <div class="col-12 col-md-6">
                <div class="card message-card">
                  <div class="card-body">
                    <form method="POST" action="{{ route('contact.store') }}" id="message-form">
                      @csrf
                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="name">
                            Your name
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="text" name="name" id="name" />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="email">
                            Email
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="email" name="email" id="email" />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="phone">
                            Telephone
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <input type="text" name="phone" id="phone" />
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                          <label for="message">
                            Message
                          </label>
                        </div>
                        <div class="col-12 col-md-8">
                            <textarea name="message" id="message"></textarea>
                        </div>
                      </div>

                      <div class="form-row">
                        <div class="col-12 col-md-4">
                        </div>
                        <div class="col-12 col-md-8">
                            <button type="submit" class="btn btn-primary">Send</button>
                        </div>
                      </div>

                  </div><!-- /card-body -->
                </div><!-- /card -->
              </div>
            </div><!-- /row -->
          </div><!-- /container -->
        </section><!-- /container-fluid -->
        <div class="front-blurb-background"></div><!-- /front-blurb-background -->


        <section id="terms" class="container-fluid front-terms-container">
          <div class="container my-4">
            <div class="row">
              <div class="col-12 col-md-6" style="margin-left: auto; margin-right: auto;">
                <h2>Terms</h2>
                <ul>
                  <li>All rates are per minute of audio/video in the file supplied</li>
                  <li>The minimum price applies to any quote where the per minute rate is lower than the minimum price</li>
                  <li>Files supplied need to be in a valid audio or video format</li>
                  <li>Turnaround times are provided on a best endevours basis. If we are unable to achieve a transcription within the turnaround time the service will be billed at the next applicable rate</li>
                  <li>Turnaround time is counted from the time that audio/video files are submitted to us until the time that we provide an invoice for services rendered and an excerpt of the work done</li>
                  <li>Full transcripts will only be provided after payment in full</li>
                </ul>
              </div>
            </div>
          </div>
        </section><!-- /terms -->

        <footer id="footer" class="container-fluid footer-container">
          <div class="container">
            <div class="row">
              <div class="col-12 col-md-6" style="margin-left: auto; margin-right: auto;">
                &copy; {{ date("Y") }}
              </div>
            </div>
          </div>
        </footer>

        <script type="text/javascript">
        // values to keep track of the number of letters typed, which quote to use. etc. Don't change these values.
        var i = 0,
            a = 0,
            isBackspacing = false,
            isParagraph = false;

        // Typerwrite text content. Use a pipe to indicate the start of the second line "|".
        var textArray = [
          "UK audio and video transcription",
          "Prices from just 80p per minute",
          "Skilled human transcibers based in the UK",
          "Risk free transcription with just one click",
          "Get a guaranteed instant price",
          "Send us your audio and video now",
        ];

        // Speed (in milliseconds) of typing.
        var speedForward = 100, //Typing Speed
            speedWait = 3000, // Wait between typing and backspacing
            speedBetweenLines = 1000, //Wait between first and second lines
            speedBackspace = 25; //Backspace Speed

        //Run the loop
        typeWriter("output", textArray);

        function typeWriter(id, ar) {
          var element = $("#" + id),
              aString = ar[a],
              eHeader = element.children("h1"), //Header element
              eParagraph = element.children("p"); //Subheader element

          // Determine if animation should be typing or backspacing
          if (!isBackspacing) {

            // If full string hasn't yet been typed out, continue typing
            if (i < aString.length) {

              // If character about to be typed is a pipe, switch to second line and continue.
              if (aString.charAt(i) == "|") {
                isParagraph = true;
                eHeader.removeClass("cursor");
                eParagraph.addClass("cursor");
                i++;
                setTimeout(function(){ typeWriter(id, ar); }, speedBetweenLines);

              // If character isn't a pipe, continue typing.
              } else {
                // Type header or subheader depending on whether pipe has been detected
                if (!isParagraph) {
                  eHeader.text(eHeader.text() + aString.charAt(i));
                } else {
                  eParagraph.text(eParagraph.text() + aString.charAt(i));
                }
                i++;
                setTimeout(function(){ typeWriter(id, ar); }, speedForward);
              }

            // If full string has been typed, switch to backspace mode.
            } else if (i == aString.length) {

              isBackspacing = true;
              setTimeout(function(){ typeWriter(id, ar); }, speedWait);

            }

          // If backspacing is enabled
          } else {

            // If either the header or the paragraph still has text, continue backspacing
            if (eHeader.text().length > 0 || eParagraph.text().length > 0) {

              // If paragraph still has text, continue erasing, otherwise switch to the header.
              if (eParagraph.text().length > 0) {
                eParagraph.text(eParagraph.text().substring(0, eParagraph.text().length - 1));
              } else if (eHeader.text().length > 0) {
                eParagraph.removeClass("cursor");
                eHeader.addClass("cursor");
                eHeader.text(eHeader.text().substring(0, eHeader.text().length - 1));
              }
              setTimeout(function(){ typeWriter(id, ar); }, speedBackspace);

            // If neither head or paragraph still has text, switch to next quote in array and start typing.
            } else {

              isBackspacing = false;
              i = 0;
              isParagraph = false;
              a = (a + 1) % ar.length; //Moves to next position in array, always looping back to 0
              setTimeout(function(){ typeWriter(id, ar); }, 50);

            }
          }
        }

        // AJAX upload the file for calculation
        $('#upload-form').on('change', function() {
          var file = $('#uploadFile')[0].files[0];

          objectUrl = URL.createObjectURL(file);
          $("#audio").prop("src", objectUrl);
        });

        // Work out the file duration
        var objectUrl;

        $("#audio").on("canplaythrough", function(e){
            // Get the file duration
            var seconds = e.currentTarget.duration;

            URL.revokeObjectURL(objectUrl);

            var formData = { '_token': '{{ csrf_token() }}',
                             'turnaround': $('#turnaround').val(),
                             'verbatim': ($('#verbatim').is(':checked') ? 'true' : 'false'),
                             'timestamps': ($('#timestamps').is(':checked') ? 'true' : 'false'),
                             'duration': seconds };

            // Get the price
            $.ajax({
              type: 'POST',
              url: '{{ route('file.calculate') }}',
              data: formData,
              success: function(data) {
                $('#duration').html(data.duration);
                $('#cost').html('£' + data.price);
              },
              dataType: 'JSON',
            });
        });

        </script>
@endsection
