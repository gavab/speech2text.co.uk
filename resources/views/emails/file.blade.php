<body>
  <p>From: {{ $username }}</p>
  <p>Email: {{ $email }}</p>
  <p>Phone: {{ $phone }}</p>
  <p>Turnaround: {{ $turnaround }}</p>
  <p>Verbatim: {{ $verbatim }}</p>
  <p>Filename: <a href="{{ $filename }}">{{ $filename }}</a></p>
  <p>Timestamps: {{ $timestamps }}</p>
  <p>Duration: {{ $duration }}</p>
  <p>Cost: {{ $cost }}</p>
</body>
