<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Speech2Text.co.uk</title>

        <script src="{{ asset('js/app.js') }}"></script>

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-139594024-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-139594024-1');
        </script>

        <!-- Facebook Pixel Code -->
        <script>
          !function(f,b,e,v,n,t,s)
          {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
          n.callMethod.apply(n,arguments):n.queue.push(arguments)};
          if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
          n.queue=[];t=b.createElement(e);t.async=!0;
          t.src=v;s=b.getElementsByTagName(e)[0];
          s.parentNode.insertBefore(t,s)}(window, document,'script',
          'https://connect.facebook.net/en_US/fbevents.js');
          fbq('init', '392471094677224');
          fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none"
          src="https://www.facebook.com/tr?id=392471094677224&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    </head>
    <body>
      <nav id="navbar">
        <div class="sitename">
          <a href="/">Speech2Text.co.uk</a>
        </div>
        <div class="sitelinks d-none d-md-block">
          <ul>
            <li><a href="#instant">Instant Quote</a></li>
            <li><a href="#pricing">Pricing</a></li>
            <li><a href="#gold-members"><span class="gold">Gold Members</a></span></li>
            <li><a href="#contact">Contact Us</a></li>
            <li><a href="#terms">Terms</a></li>
          </ul>
        </div>
        <div class="mobile-menu d-block d-md-none">
          <i class="fa fa-bars"> </i>
        </div>
        <div class="mobile-menu-links" style="display: none">
          <ul>
            <li><a href="#instant">Instant Quote</a></li>
            <li><a href="#pricing">Pricing</a></li>
            <li><a href="#gold-members"><span class="gold">Gold Members</a></span></li>
            <li><a href="#contact">Contact Us</a></li>
            <li><a href="#terms">Terms</a></li>
          </ul>
        </div>
      </nav>
      <div class="flash-message {{ (isset($flashClass) || $errors->any() ? '' : 'd-none') }}">
        <div class="card">
          <div class="card-body {{ (isset($flashClass) ? $flashClass : '') }} {{ ($errors->any() ? 'alert-danger' : '') }}">
            @if(isset($flashMessage) && $flashMessage == 'Your file has been uploaded for transcription')
              <script>
                fbq('track', 'Lead');
              </script>
            @endif

            @if(isset($flashMessage) && $flashMessage == 'Your message has been sent')
              <script>
                fbq('track', 'Contact');
              </script>
            @endif

            {{ (isset($flashMessage) ? $flashMessage : '') }}
            @if($errors->any())
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            @endif
            <div class="flash-close">
              <a href="javascript:void(0)">&times;</a>
            </div>
          </div>
        </div>
      </div>
      @yield('content')
    </body>
    <script type="text/javascript">
    // Scroll to on link click
    $("a[href^='#']").click(function(e) {
      e.preventDefault();
      var dest = $(this).attr('href');
      $('html,body').animate({ scrollTop: $(dest).offset().top }, 'slow');
    });

    // Close flash message
    $('.flash-close a').on('click', function() {
      $('.flash-message').hide();
    });

    // Mobile menu
    $('.mobile-menu').on('click', function() {
      if ($('.mobile-menu-links').is(':visible')) {
        $('.mobile-menu-links').hide();
        $('.mobile-menu').html('<i class="fa fa-bars"> </i>');
      } else {
        $('.mobile-menu-links').show();
        $('.mobile-menu').html('<i class="fa fa-times"> </i>');
      }
    });
    $('.mobile-menu-links').on('click', function() {
      $('.mobile-menu-links').hide();
      $('.mobile-menu').html('<i class="fa fa-bars"> </i>');
    });
    </script>
</html>
