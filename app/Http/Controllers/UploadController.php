<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use URL;
use getID3;
use Mail;

class UploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validate the data
      $validationData = $request->validate([
        'uploadFile' => 'mimes:ogg,oga,wav,mpga,mp2,mp2a,mp3,m2a,m3a,mpeg,mpa,mp4a,m4a,au,aac,flac,ogv,mp4,mpeg'
      ]);

      // Get the details
      $filename = $request->file('uploadFile')->getPathName();

      // Get the duration
      $getID3 = new getID3;
      $file = $getID3->analyze($filename);
      $duration = $file['playtime_seconds'];
      $duration_string = $file['playtime_string'];

      // Set the price
      switch ($request->turnaround) {
        case 0:
          $rate = 3.2;
          $ta = '5 hours';
          break;
        case 1:
          $rate = 2.5;
          $ta = '1 day';
          break;
        case 2:
          $rate = 1.7;
          $ta = '2 days';
          break;
        case 3:
          $rate = 1.1;
          $ta = '3 days';
          break;
        case 4:
          $rate = 0.92;
          $ta = '4 days';
          break;
      }

      // Check for extras
      if ($request->verbatim == 'true')
        $rate += 0.15;

      if ($request->timestamps == 'true')
        $rate += 0.15;

      // Get the price from that
      $price = $duration / 60 * $rate;
      $price = number_format($price, 2);

      // Minimum price
      if ($price < 2.99) $price = 2.99;

      // Save the file
      $filePath = $request->file('uploadFile')->store('public/client_files');

      // Send the mail
      Mail::send('emails.file', [    'username' => $request->name,
                                     'email' => $request->email,
                                     'phone' => $request->phone,
                                     'turnaround' => $ta,
                                     'verbatim' => $request->verbatim,
                                     'timestamps' => $request->timestamps,
                                     'filename' => URL::to('/') . Storage::url($filePath),
                                     'cost' => $price,
                                     'duration' => $duration_string ], function($message) use ($request) {
                                       $message->from('info@speech2text.co.uk');
                                       $message->to('info@speech2text.co.uk');
                                       $message->to('gavin.abson@gmail.com');
                                       $message->subject('New File for Transcription');
                                       $message->attach($request->file('uploadFile')->getPathName(), [ 'as' => $request->file('uploadFile')->getClientOriginalName() ]);
                                     });

      // Redirect back with flash message
      return view('welcome')->with([ 'flashMessage' => 'Your file has been uploaded for transcription', 'flashClass' => 'alert-success' ]);    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Calculate the size of the file
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function calculate(Request $request)
    {
        // // Get the details
        // $filename = $request->file('uploadFile')->getPathName();
        //
        // // Get the duration
        // $getID3 = new getID3;
        // $file = $getID3->analyze($filename);
        // $duration = $file['playtime_seconds'];
        // $duration_string = $file['playtime_string'];

        // Duration in seconds
        $duration = floor($request->duration);
        $duration_string = gmdate('H:i:s', $duration);

        // Set the price
        switch ($request->turnaround) {
          case 0:
            $rate = 3.2;
            break;
          case 1:
            $rate = 2.5;
            break;
          case 2:
            $rate = 1.7;
            break;
          case 3:
            $rate = 1.1;
            break;
          case 4:
            $rate = 0.92;
            break;
        }

        // Check for extras
        if ($request->verbatim == 'true')
          $rate += 0.15;

        if ($request->timestamps == 'true')
          $rate += 0.15;

        // Get the price from that
        $price = $duration / 60 * $rate;
        $price = number_format($price, 2);

        // Minimum price
        if ($price < 2.99) $price = 2.99;

        return response()->json([ 'duration' => $duration_string, 'price' => $price ]);
    }
}
